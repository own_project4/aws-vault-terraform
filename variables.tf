variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "name" { default = "dynamic-aws-creds-vault-admin" }
variable "path" { default = "terraform.tfstate"}
variable "region" { default = "eu-west-3"}