If you are working on a staging or production environment then use the Server Mode of the HashiCorp Vault to start your server. Follow the below steps for starting the HashiCorp vault -

Before starting the HashiCorp Vault server create a config file at a suitable location (ex. /home/vagrant/vault-config/config/file)

        mkdir -p path/file/location #remember to change this path location with your own
        touch config.hcl
        mkdir -p ./vault/data #remember to change this path location of your data with your own

# Add the following configuration to the file -
==> config.hcl

                storage "file" {
                path    = "./vault/data"
                node_id = "node1"
                }

                listener "tcp" {
                address     = "127.0.0.1:8200"
                tls_disable = "true"

                telemetry {
                unauthenticated_metrics_access = true
                }
                }

                disable_mlock = true

                api_addr = "http://127.0.0.1:8200"
                cluster_addr = "https://127.0.0.1:8201"
                ui = true


# Initializing the Vault
Open new terminal, and set the env variable
        export VAULT_ADDR='http://127.0.0.1:8200'

Then initialize vault
        vault operator init


3. Export AWS Secrets, HashiCorp VAULT_ADDR, and HashiCorp VAULT_TOKEN
In the previous, we have installed the HashiCorp Vault server but to work with AWS Secrets we need to set some environment variables -

TF_VAR_aws_access_key - The AWS Access Key
TF_VAR_aws_secret_key - The AWS Secret Key
VAULT_ADDR - The HashiCorp Vault server address (.i.e. - http://127.0.0.1:8200)
VAULT_TOKEN - The Root Token which we have generated when starting the HashiCorp Server.
Here are the commands for exporting the environment variables -

                export TF_VAR_aws_access_key=
                export TF_VAR_aws_secret_key=
                export VAULT_ADDR=http://127.0.0.1:8200
                export VAULT_TOKEN=


# Seal/Unseal

you've got keys after running "vault operator init"
You will have to unseal three (03) of them with the command:

        vault operator unseal

Every initialized Vault server starts in the sealed state. From the configuration, Vault can access the physical storage, but it can't read any of it because it doesn't know how to decrypt it. The process of teaching Vault how to decrypt the data is known as unsealing the Vault.

Unsealing has to happen every time Vault starts. It can be done via the API and via the command line. To unseal the Vault, you must have the threshold number of unseal keys. In the output above, notice that the "key threshold" is 3. This means that to unseal the Vault, you need 3 of the 5 keys that were generated.


4. Add AWS Secrets inside HashiCorp Vault
Let's write some terraform script to implement secure dynamically generated credentials. For this blog post, we are going to create an S3 Bucket using the dynamically generated AWS credentials -

4.1 Setup AWS Engine to generate AWS Secrets which are valid for 2 minutes
As I mentioned early in the post about secure we should generate short-lived AWS Secrets. So let's create AWS resource vault_aws_secret_backend.aws in which we are going to define -

default lease time : 120 Seconds( 2 min )
max lease time : 240 Seconds( 4 min )

Here is the code block for vault_aws_secret_backend -

                resource "vault_aws_secret_backend" "aws" {
                access_key = var.aws_access_key
                secret_key = var.aws_secret_key
                path       = "${var.name}-path"

                default_lease_ttl_seconds = "120"
                max_lease_ttl_seconds     = "240"
                }

        storage "file" {
        path = "vault/data"
        }

        listener "tcp" {
        address = "127.0.0.1:8200"
        tls_disable = 1
        }
4.2 Setup IAM roles only for S3 Bucket
The next best practice for securing AWS Credentials is to assign the least possible IAM roles. As we are going to create an S3 Bucket so the IAM role should only have S3 roles.

        ui = true
Here is the terraform resource block with only S3 IAM roles -

                resource "vault_aws_secret_backend_role" "admin" {
                backend         = vault_aws_secret_backend.aws.path
                name            = "${var.name}-role"
                credential_type = "iam_user"

                policy_document = <<EOF
                {
                "Version": "2012-10-17",
                "Statement": [
                {
                "Effect": "Allow",
                "Action": [
                        "iam:*", "s3:*"
                ],
                "Resource": "*"
                }
                ]
                }
                EOF
                }
